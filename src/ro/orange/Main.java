package ro.orange;

public class Main {

    public static void main(String[] args) {
	    // class variables definition
        boolean boo = true;
	    char cha = 'A';
	    String[] str = {"A", "B", "C"};
	    int in = 88;
	    float flo = 3.5F;
	    double dou = -9.3d;
	    long lon = 9000000;

        System.out.println(boo);
        System.out.println(cha);
        for (String item:str) System.out.print(item);
        System.out.println();
        System.out.println(flo);
        System.out.println(dou);
        System.out.println(in);
        System.out.println(lon);
        for (String item:str) System.out.print(item.toLowerCase());

    }
}
